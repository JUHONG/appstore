//
//  API.swift
//  Appstore
//
//  Created by 이주홍 on 2018. 4. 30..
//  Copyright © 2018년 이주홍. All rights reserved.
//

import UIKit

final class API {
    
    private let defaultSession = URLSession(configuration: .default)
    
    /// 앱 목록/상세 데이터 가져오기
    func getData(_ url:String, completion:@escaping (_ response: [String:Any]) -> Void){
        
        let request = URLRequest(url: URL(string: url)!)
        
        let dataTask = defaultSession.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                print("Error occur: \(String(describing: error))")
                return
            }
            
            if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                
                guard let jsonToArray = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any] else {
                    return
                }
                
                completion(jsonToArray)
            }
        }
        
        dataTask.resume()
        
    }
    
    /// 이미지 가져오기
    func getImage(_ url:String, completion:@escaping (_ image: UIImage) -> Void){
        
        let dataTask = defaultSession.dataTask(with: URL(string: url)!) { (data, response, error) in
            
            guard error == nil else {
                print("Error occur: \(String(describing: error))")
                return
            }
            
            if let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                
                if let imageData = data {
                    
                    let image = UIImage(data: imageData)
                    
                    completion(image!)
                    
                }
            }
        }
        
        dataTask.resume()
    }

}
