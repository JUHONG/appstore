//
//  APIHelper.swift
//  Appstore
//
//  Created by 이주홍 on 2018. 4. 30..
//  Copyright © 2018년 이주홍. All rights reserved.
//

import UIKit

class APIHelper {
    
    static let shared = APIHelper()
    private let dataManager = DataManager()
    private let api = API()
    public var selectedID:String = ""
    public var selectedIdx:Int = 0
    
    private init() {}
    
    /// 앱 리스트 가져오기
    func getApps(completion: @escaping (_ appData:[App])->Void){
        let url = "https://itunes.apple.com/kr/rss/topfreeapplications/limit=50/genre=6015/json"
        api.getData(url){ response in
            
            if let feed = response["feed"] as? [String : Any],
                let entrys = feed["entry"] {
                
                for entry in entrys as! [[String:Any]] {
                    self.dataManager.addApp(entry)
                }
            }
            
            completion(self.dataManager.getApps())
        }
    }
    
    /// 앱 상세정보 가져오기
    func getAppDetail(completion: @escaping (_ app:App)->Void){
    
        let url = "https://itunes.apple.com/lookup?id=\(self.selectedID)&country=kr"
        print(url)
        api.getData(url){ response in
            
            if let results = response["results"]{
                
                for result in results as! [[String:Any]] {
                    self.dataManager.addAppDetail(result, idx:self.selectedIdx)
                }
            }

            completion(self.dataManager.getSelectedApp(self.selectedIdx))
        }
    }
    
    /// 로고 이미지 가져오기
    func getImage(_ idx:Int, url:String, completion: @escaping (_ image:UIImage)->Void){
        
        /// 기기에 저장된 이미지 존재 여부 확인
        if let img = dataManager.getImage(idx) {
            completion(img)
        }else{
            api.getImage(url){ image in
                
                /// 이미지 저장
                do {
                    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    let fileURL = documentsURL.appendingPathComponent("image/\(self.dataManager.getID(idx)).png")
                    if let pngImageData = UIImagePNGRepresentation(image) {
                        try pngImageData.write(to: fileURL, options: .atomic)
                    }
                } catch { }
                
                self.dataManager.setImage(idx, image: image)
                
                completion(image)
            }
        }
    }
    
    /// 앱 상세화면의 스크린샷 가져오기
    func getScreenshotImage(_ url:String,completion: @escaping (_ image:UIImage)->Void){
        
        /// 저장된 이미지 존재여부 확인
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsURL.appendingPathComponent("image/\(url).png")
        if FileManager.default.fileExists(atPath:filePath.path) {
            if let img = UIImage(contentsOfFile:filePath.path){
                completion(img)
                return
            }
        }
        
        /// 이미지 다운로딩 후 기기에 저장
        api.getImage(url){ image in
            
            /// 이미지 저장
            do {
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let fileURL = documentsURL.appendingPathComponent("image/\(url).png")
                if let pngImageData = UIImagePNGRepresentation(image) {
                    try pngImageData.write(to: fileURL, options: .atomic)
                }
            } catch { }
            
            completion(image)
        }
        
    }
        
}


