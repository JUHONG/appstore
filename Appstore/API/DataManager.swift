//
//  DataManager.swift
//  Appstore
//
//  Created by 이주홍 on 2018. 4. 30..
//  Copyright © 2018년 이주홍. All rights reserved.
//

import UIKit

final class DataManager {
    
    private var apps = [App]()
    
    init(){}
    
    /// 앱 리스트 가져오기
    func getApps() -> [App]{
        return apps
    }
    
    /// 앱 리스트 추가하기
    func addApp(_ data: [String:Any]){
        
        var title = ""
        var image = ""
        var subLabel = ""
        var priceLabel = ""
        var id = ""
        
        if let name = data["im:name"] as? [String:String],
            let label = name["label"] {
            
            title = label
        }
        
        if let data = data["im:image"] as? [[String:Any]],
            let img = data[2]["label"] {
            image = img as! String
        }
        
        if let data = data["category"] as? [String:Any],
            let sub = data["attributes"] as? [String:String],
            let lb = sub["label"] {
            subLabel = lb
        }

        if let data = data["im:price"] as? [String:Any],
            let lb = data["label"] {
            priceLabel = lb as! String
        }
        
        if let data = data["id"] as? [String:Any],
            let attributes = data["attributes"] as? [String:String],
            let imid = attributes["im:id"] {
            id = imid
        }
        
        if title.count != 0,
           image.count != 0,
            subLabel.count != 0,
            priceLabel.count != 0 {
            apps.append(App(title:title,imgPath:image,subLabel:subLabel,priceLabel:priceLabel, id:id))
            
        }
    }
    
    /// 로고 이미지 가져오기
    func getImage(_ idx:Int) -> UIImage?{
        return apps[idx].img
    }
    
    /// 특정 앱 목록의 ID 가져오기
    func getID(_ idx:Int) -> String{
        return apps[idx].id
    }
    
    /// 로고 이미지 저장하기
    func setImage(_ idx:Int, image:UIImage){
        apps[idx].img = image
    }
    
    /// 앱 상세화면 저장하기
    func addAppDetail(_ data: [String:Any], idx:Int){
        
        print(data)
        
        var title = ""
        var category = ""
        var rating = ""
        var subRating = ""
        var age = ""
        var seller = ""
        var size = ""
        var imgs = [Image]()
        
        if let name = data["trackName"] {
            title = name as! String
        }

        if let cate = data["primaryGenreName"]{
            category = cate as! String
        }

        if let rate = data["averageUserRating"]{
            rating = "\(rate)"
        }
        
        if let subRate = data["userRatingCount"]{
            subRating = "\(subRate) Ratings"
        }
        
        if let agee = data["contentAdvisoryRating"]{
            age = agee as! String
        }
        
        if let sellerName = data["sellerName"]{
            seller = sellerName as! String
        }
        
        if let byte = data["fileSizeBytes"]{
            size = "\(byte)"
            let tmp = size
            let indexEndOfText = tmp.index(tmp.endIndex, offsetBy: -7)
            let substring = tmp[...indexEndOfText]  // "Hello>>>"
            size = "\(substring) MB"
            print(size)
        }
        
        if let images = data["screenshotUrls"]{
            
            for image in images as! [String] {
                imgs.append(Image(path:image, image:nil))
            }
        }
        

        if title.count != 0,
            category.count != 0,
            rating.count != 0,
            subRating.count != 0,
            age.count != 0,
            seller.count != 0,
            size.count != 0{
            
            apps[idx].appDetail = AppDetail(title: title, category: category, rating:rating, subRating: subRating, age: age, images:imgs, seller:seller, size:size)

        }
    }
    
    /// 앱 상세화면 가져오기
    func getAppDetail(_ idx:Int)->AppDetail?{
        if let detail = apps[idx].appDetail{
            return detail
        }
        
        return nil

    }
    
    /// 특정 앱 목록 가져오기
    func getSelectedApp(_ idx:Int)->App{
        return apps[idx]
    }
}


