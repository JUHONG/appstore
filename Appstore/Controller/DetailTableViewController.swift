//
//  DetailTableViewController.swift
//  Appstore
//
//  Created by LEEJUHONG on 2018. 5. 1..
//  Copyright © 2018년 LEEJUHONG. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {
    
    private var app:App?
    private var infoTitle:[String] = ["Seller","Size","Category","Age Rating"]
    private var infoVal:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /// 앱 상세화면 데이터 가져오기
        APIHelper.shared.getAppDetail(){app in
            
            self.app = app
            self.infoVal = [self.app?.appDetail?.seller,
                            self.app?.appDetail?.size,
                            self.app?.appDetail?.category,
                            self.app?.appDetail?.age] as! [String]
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}

extension DetailTableViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 200
        case 1:
            return 400
        default:
            return 50
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    

     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:

             let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCustomCell", for: indexPath) as! InfoCustomCell
             
            if let appDetail = self.app?.appDetail {
                cell.title.text = appDetail.title
                cell.subTitle.text = appDetail.category
                cell.category.text = appDetail.category
                cell.age.text = appDetail.age
                cell.rating.text = appDetail.rating
                cell.idx.text = "#\(APIHelper.shared.selectedIdx+1)"
                cell.imgV.image = self.app?.img
            }

            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PreviewCustomCell", for: indexPath) as! PreviewCustomCell

            if let appDetail = self.app?.appDetail {

                var idx:Int = 0

                for image in (appDetail.images)!{

                    APIHelper.shared.getScreenshotImage(image.path){ img in

                        DispatchQueue.main.async {
                            let imgV = UIImageView.init(image: img)
                            let posX = (250 * idx)+5

                            imgV.frame = CGRect(x: posX,
                                                y: 0,
                                                width: 250,
                                                height: Int(cell.frame.size.height))
                            imgV.contentMode = UIViewContentMode.scaleAspectFit

                            cell.scrollview.addSubview(imgV)

                            cell.scrollview.contentSize = CGSize(width:Int(self.tableView.frame.size.width)*idx, height:Int(cell.frame.size.height))

                            idx += 1
                        }
                    }
                }
            }

            return cell

        default:

            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

            if let _ = self.app?.appDetail {
                let row = indexPath.row-2

                if self.infoVal[row].count != 0{
                    cell.detailTextLabel?.text = self.infoVal[row]

                    cell.textLabel?.text = self.infoTitle[row]
                    cell.textLabel?.textColor = UIColor.lightGray
                    cell.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
                }
            }
            return cell

        }
     }
    
}
