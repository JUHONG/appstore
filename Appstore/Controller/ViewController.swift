//
//  ViewController.swift
//  Appstore
//
//  Created by LEEJUHONG on 2018. 4. 29..
//  Copyright © 2018년 LEEJUHONG. All rights reserved.
//

import UIKit



class ViewController: UITableViewController {
    
    private var apps: [App]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Top Charts"
        
        /// 앱 목록 데이터 가져오기
        APIHelper.shared.getApps(){appData in
            self.apps = appData
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let app = apps else {
            return 0
        }
        return app.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppCustomCell", for: indexPath) as! AppCustomCell

        if let app = apps, app.count != 0 {
            let row = indexPath.row
            
            APIHelper.shared.getImage(row, url: app[row].imgPath){image in
                DispatchQueue.main.async {
                    cell.imgV.image = image
                }
            }
            cell.title.text = app[row].title
            cell.subTitle.text = app[row].subLabel
            cell.downLabel.text = app[row].priceLabel
            
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if let app = apps, app.count != 0 {
            let row = indexPath.row
            
            APIHelper.shared.selectedID = app[row].id
            APIHelper.shared.selectedIdx = row
        }
        
        return indexPath
    }
    
}
