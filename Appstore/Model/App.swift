//
//  App.swift
//  Appstore
//
//  Created by 이주홍 on 2018. 4. 30..
//  Copyright © 2018년 이주홍. All rights reserved.
//

import UIKit

struct App {
    
    let title: String
    let imgPath: String
    let subLabel: String
    let priceLabel: String
    let id: String
    var img: UIImage?
    var appDetail:AppDetail?
    
    init(title:String, imgPath:String, subLabel:String, priceLabel:String, id:String ){
        self.title = title
        self.imgPath = imgPath
        self.subLabel = subLabel
        self.priceLabel = priceLabel
        self.id = id
        
        img = nil
        appDetail = nil
        
        /// 기기에 저장된 이미지 있으면 불러옴
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsURL.appendingPathComponent("image/\(id).png")
        if FileManager.default.fileExists(atPath:filePath.path) {
            if let img = UIImage(contentsOfFile:filePath.path){
                self.img = img
            }
        }
    }
}

