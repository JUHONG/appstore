//
//  AppDetail.swift
//  Appstore
//
//  Created by LEEJUHONG on 2018. 5. 1..
//  Copyright © 2018년 LEEJUHONG. All rights reserved.
//

import UIKit

struct AppDetail {
    
    let title: String
    let category: String
    let rating: String
    let subRating: String
    let age: String
    let seller: String
    let size: String
    let images: [Image]?
    
    init(title: String, category: String, rating:String, subRating: String, age: String, images:[Image], seller:String, size:String){
        self.title = title
        self.category = category
        self.rating = rating
        self.subRating = subRating
        self.age = age
        self.images = images
        self.seller = seller
        self.size = size
    }
}

struct Image {
    let path: String
    var image: UIImage?
}
