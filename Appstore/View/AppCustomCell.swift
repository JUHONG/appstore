//
//  AppCustomCell.swift
//  Appstore
//
//  Created by LEEJUHONG on 2018. 5. 1..
//  Copyright © 2018년 LEEJUHONG. All rights reserved.
//

import UIKit

class AppCustomCell: UITableViewCell {

    @IBOutlet weak var imgV: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var downLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
