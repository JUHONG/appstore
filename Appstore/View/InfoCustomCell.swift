//
//  InfoCustomCell.swift
//  Appstore
//
//  Created by LEEJUHONG on 2018. 5. 1..
//  Copyright © 2018년 LEEJUHONG. All rights reserved.
//

import UIKit

class InfoCustomCell: UITableViewCell {

    @IBOutlet weak var imgV: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var idx: UILabel!
    @IBOutlet weak var category: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
